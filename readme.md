# ddRAD-seq pipeline

## Installation

The following code will download the pipeline and dependencies:
```sh
git clone --recursive https://gitlab.com/IGBB/pipelines/ddrad-seq.git
make -C ddrad-seq/ -f setup.mk
```
### Dependencies 
 - Containers:
   - bwa v0.7.17  
   quay.io/biocontainers/bwa\:0.7.17--h5bf99c6_8
   - samtools v1.13  
   quay.io/biocontainers/samtools\:1.13--h8c37831_0
   - bedtools v2.30.0  
   quay.io/biocontainers/bedtools\:2.30.0--h7d7f7ad_2
   - picard v2.26.3  
   quay.io/biocontainers/picard\:2.26.3--hdfd78af_0
   - bcftools v1.13  
   quay.io/biocontainers/bcftools\:1.13--h3a49de5_0
   - gatk v4.2.2.0  
   broadinstitute/gatk\:4.2.2.0
   - vcftools v0.1.16  
   quay.io/biocontainers/vcftools\:0.1.16--h9a82719_5
 - Bare-metal:
   - egads  
   https://github.com/IGBB/egads 
   - plink v1.07  
   https://www.cog-genomics.org/plink/1.9/

## Running (Single Machine)

- `NAME`  
Name of results
- `GENOME`  
Reference genome
- `LIBS`  
list of library variables. Each name in the list should reference a variable with the space
delimited raw files (fastq).
- `CPUS`  
Number of CPUs to use per task
- `RARE`  
Name of rare cutting digestions enzyme
- `FREQ`  
Name of frequent cutting digestions enzyme

```sh
make -f ddrad-seq/makefile -j 10 \
    NAME=human \
    GEOME=GCF_000001405.39_GRCh38.p13_genomic.fna.gz \
    CPUS=10 \
    RARE=HindIII \
    FREQ=Sau3AI \
    LIBS='lib1 lib2 lib3'\
    lib1="raw/lib1_1.fq raw/lib1_2.fq" \
    lib2="raw/lib2_1.fq raw/lib2_2.fq" \
    lib3="raw/lib3_1.fq raw/lib3_2.fq" \
    all
```


## Running (SLURM)

This feature is currently untested for this pipeline and abuses `make`, so:
> Lasciate ogne speranza, voi ch'intrate

#### Submit Jobs
```sh
make -f ddrad-seq/makefile \
    NAME=human \
    GEOME=GCF_000001405.39_GRCh38.p13_genomic.fna.gz \
    RARE=HindIII \
    FREQ=Sau3AI \
    LIBS='lib1 lib2 lib3'\
    lib1="raw/lib1_1.fq raw/lib1_2.fq" \
    lib2="raw/lib2_1.fq raw/lib2_2.fq" \
    lib3="raw/lib3_1.fq raw/lib3_2.fq" \
    queue
```

The jobs are held for review/modification. To release all of them, run `scontrol
release $(ar p $NAME.queue.a)`. The command will remove the hold for all job ids
in the `$NAME.queue.a` file.

Use the he `queue_extra` variable to add anything to the `sbatch` commands
(account, partition, etc.).

#### Check Progress
```sh
make -f ddrad-seq/queue.mk NAME=human show
```


#### Re-queue Failed Jobs
```sh
make -f ddrad-seq/makefile \
    NAME=human \
    GEOME=GCF_000001405.39_GRCh38.p13_genomic.fna.gz \
    RARE=HindIII \
    FREQ=Sau3AI \
    LIBS='lib1 lib2 lib3'\
    lib1="raw/lib1_1.fq raw/lib1_2.fq" \
    lib2="raw/lib2_1.fq raw/lib2_2.fq" \
    lib3="raw/lib3_1.fq raw/lib3_2.fq" \
    requeue
```

