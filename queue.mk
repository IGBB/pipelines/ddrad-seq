#################################################################################
# SLURM Queueing Makefile
#
# @file
# @version 0.1
#
############ Caveats and Assumptions ############################################
#
#   1) Using the ar tool as a database. Can't use with parallel make (-j)
#
#   2) Using pregenerated rule for ar, so assuming everything needs to look like
#      an object file (i.e *.o). ASSUMPTION NOT TESTED
#
#   3) Everything is queued. Job exits 'ok' immediately if already completed
#
#################################################################################
pipeline := $(dir $(abspath $(lastword $(MAKEFILE_LIST))))

include $(pipeline)/config.mk

# queue id database
queued:= $(NAME).queue.a

# queuing command
queue_cmd := sbatch --exclusive=user --parsable --hold

create_queue_resources = \
					$(if $($1-nodes), --nodes=$($1-nodes)) \
					$(if $($1-tasks), --ntasks=$($1-tasks)) \
					$(if $($1-cpus),  --cpus-per-task=$($1-cpus)) \
					$(if $($1-time),  --time=$($1-time)) \
					$(if $($1-mem),  --mem=$($1-mem)) \
					$(if $($1-partition), --partition=$($1-partition)) \
					$(if $($1-array),     --array=$($1-array))

#### Prepare
sections := prepare.o
prepare-resources-tasks := 1
prepare-resources-cpus  := 1
prepare-resources-time  := 30:00
prepare.o:

#### BWA
sections += bwa-setup.o \
			bwa-run.o

bwa-setup-resources-tasks := 1
bwa-setup-resources-cpus  := 1
bwa-setup-resources-time  := 2:00:00
bwa-setup.o:

bwa-run-resources-tasks := 1
bwa-run-resources-cpus  := 12
bwa-run-resources-time  := 48:00:00
bwa-run-resources-mem   := 10G
bwa-run-resources-array := 1-$(words $(LIBS))
bwa-run.o: $(queued)(bwa-setup.o)

#### egads
sections += egads.o

egads-resources-tasks := 1
egads-resources-cpus  := 1
egads-resources-time  := 2:00:00
egads.o: $(queued)(prepare.o)

#### TODO: coverage

#### markdups
sections += snps-markdups-run.o
snps-markdups-run-resources-tasks := 1
snps-markdups-run-resources-cpus  := 1
snps-markdups-run-resources-time  := 8:00:00
snps-markdups-run-resources-mem   := 10G
snps-markdups-run-resources-array := 1-$(words $(LIBS))
snps-markdups-run.o: $(queued)(bwa-run.o)

#### prelim
sections += snps-prelim.o
snps-prelim-resources-tasks := 1
snps-prelim-resources-cpus  := 1
snps-prelim-resources-time  := 48:00:00
snps-prelim.o : $(queued)(snps-markdups-run.o)

#### bqsr
sections += snps-bqsr-setup.o snps-bqsr-run.o

snps-bqsr-setup-resources-tasks := 1
snps-bqsr-setup-resources-cpus  := 1
snps-bqsr-setup-resources-time  := 1:00:00
snps-bqsr-setup.o: $(queued)(prepare.o snps-prelim.o)

snps-bqsr-run-resources-tasks := 1
snps-bqsr-run-resources-cpus  := 8
snps-bqsr-run-resources-time  := 24:00:00
snps-bqsr-run-resources-mem   := 10G
snps-bqsr-run-resources-array := 1-$(words $(LIBS))
snps-bqsr-run.o: $(queued)(snps-bqsr-setup.o snps-markdups-run.o snps-prelim.o)

#### haplotype
sections += snps-haplotype-run.o
snps-haplotype-run-resources-tasks := 1
snps-haplotype-run-resources-cpus  := 16
snps-haplotype-run-resources-time  := 48:00:00
snps-haplotype-run-resources-mem   := 50G
snps-haplotype-run-resources-array := 1-$(words $(LIBS))
snps-haplotype-run.o: $(queued)(snps-bqsr-run.o)

#### combine
sections += snps-combine-run.o
snps-combine-run-resources-tasks := 1
snps-combine-run-resources-cpus  := 16
snps-combine-run-resources-time  := 48:00:00
snps-combine-run-resources-mem   := 50G
snps-combine-run-resources-array := 1-$(words $(GENOME_SCAFFOLDS))
snps-combine-run.o: $(queued)(snps-haplotype-run.o)

#### genotype
sections += snps-genotype-run.o
snps-genotype-run-resources-tasks := 1
snps-genotype-run-resources-cpus  := 1
snps-genotype-run-resources-time  := 48:00:00
snps-genotype-run-resources-mem   := 50G
snps-genotype-run-resources-array := 1-$(words $(GENOME_SCAFFOLDS))
snps-genotype-run.o: $(queued)(snps-combine-run.o)

#### merge
sections += snps-merge.o
snps-merge-resources-tasks := 1
snps-merge-resources-cpus  := 1
snps-merge-resources-time  := 48:00:00
snps-prelim.o : $(queued)(snps-combine-run.o)


all : $(queued)(snps-merge.o)

requeue-failed:
	sacct -X --format='JobName%100,State' -j $$(ar p $(queued) | tr '\n' ',') \
    | awk '$$2 == "FAILED" { print $$1 ".o" }' \
    | xargs --no-run-if-empty ar d $(queued)
	sacct -X --format='JobID%25,State' -j $$(ar p $(queued) | tr '\n' ',') \
    | awk '$$2 == "PENDING" { print $$1 }' \
    | xargs --no-run-if-empty scancel
	$(MAKE) $(MFLAGS) -f $(abspath $(lastword $(MAKEFILE_LIST))) all

show :
	sacct -X --format='Comment,JobName%30,JobID,State,Start,End' \
		-j $$(ar p $(queued) | tr '\n' ',')

# Does resources variable contain --array
is-array = $(findstring --array,$(call create_queue_resources,$1-resources))

# Set output name, omitting array id number if not array
output-name = $(if $(call is-array,$1),\
					"slurm-%A-%a.$1.out",\
					"slurm-%j.$1.out")

# Get the slurm job ids from the archive
# - $(shell $(AR) p) prints the saved sbatch output to stdout
# - $(strip ) removes leading/trailing whitespace
# - $(subst $(space),: ) replaces space with ':' for use in sbatch
dep-list = $(if $1,--dependency=afterok:$(subst $(space),:,$(strip \
			$(shell $(AR) p $(queued) $1))))

#Setup task with array
task = $1$(if $(call is-array,$1),-$$SLURM_ARRAY_TASK_ID)
# delete .o files after finished
.INTERMEDIATE: $(sections)
# export all variables given to make
.EXPORT_ALL_VARIABLES:
$(sections) : %.o :
#	$(MAKE) -f $(pipeline)/annotate.mf $(MFLAGS) -q $* &&
		$(queue_cmd) $(queue_extra) \
			$(call create_queue_resources,$*-resources) \
			$(call dep-list,$(strip $^)) \
			--job-name $* \
			--comment "$(NAME)" \
			--output $(call output-name,$*) \
			--error  $(call output-name,$*) \
			--export=ALL \
			--wrap '$(MAKE) $(MFLAGS) -j $$SLURM_NTASKS CPUS=$$SLURM_CPUS_PER_TASK \
					-f $(pipeline)/makefile $(call task,$*)' > $@
