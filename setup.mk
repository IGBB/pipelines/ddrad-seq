pipeline := $(dir $(abspath $(lastword $(MAKEFILE_LIST))))

include $(pipeline)/config.mk

.PHONY: all containers egads plink
all: containers egads plink
################################################################################
## Pull Containers
################################################################################
containers: $(bwa) $(samtools) $(bedtools) $(picard) $(bcftools) $(gatk) $(vcftools)

# container uri
.PHONY: \
  quay.io/biocontainers/bwa\:0.7.17--h5bf99c6_8      \
  quay.io/biocontainers/samtools\:1.13--h8c37831_0   \
  quay.io/biocontainers/bedtools\:2.30.0--h7d7f7ad_2 \
  quay.io/biocontainers/picard\:2.26.3--hdfd78af_0   \
  quay.io/biocontainers/bcftools\:1.13--h3a49de5_0   \
  broadinstitute/gatk\:4.2.2.0                       \
  quay.io/biocontainers/vcftools\:0.1.16--h9a82719_5

$(pipeline)/bwa-0.7.17.sif     : quay.io/biocontainers/bwa\:0.7.17--h5bf99c6_8
$(pipeline)/samtools-1.13.sif  : quay.io/biocontainers/samtools\:1.13--h8c37831_0
$(pipeline)/bedtools-2.30.0.sif: quay.io/biocontainers/bedtools\:2.30.0--h7d7f7ad_2
$(pipeline)/picard-2.26.3.sif  : quay.io/biocontainers/picard\:2.26.3--hdfd78af_0
$(pipeline)/bcftools-1.13.sif  : quay.io/biocontainers/bcftools\:1.13--h3a49de5_0
$(pipeline)/gatk-4.2.2.0.sif   : broadinstitute/gatk\:4.2.2.0
$(pipeline)/vcftools-0.1.16.sif: quay.io/biocontainers/vcftools\:0.1.16--h9a82719_5
%.sif:
	$(sload) &&	singularity pull $@ docker://$^

################################################################################
## Build egads
################################################################################
egads: $(pipeline)/egads/egads
$(pipeline)/egads/egads:
	$(MAKE) $(MFLAGS) -C $(pipeline)/egads/src all


################################################################################
## Download plink
################################################################################
$(pipleine)/plink-1.07.zip:
	wget -O $@ https://s3.amazonaws.com/plink1-assets/1.07/plink1_linux_x86_64.zip

plink: $(pipeline)/plink-1.07.zip
	unzip $<
