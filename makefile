################################################################################
# ddRAD-seq analysis pipeline
#
# @file
# @version 0.1
#
############ Caveats and Assumptions
#
#	1) bwa pulls the platform unit (PU) from filename of forward reads. It
#	   assumes the filename is in the format from novogene.
#
#       Sample ID        ?Machine ID?      Flowcell  Lane    Pair
#             |	             |                    |     \   /
#  	          H1_CKDL200165063-1a-N701-AK1827_HCJJMCCX2_L1_1.fq.gz
#
###############################################################################

# Path to pipeline makefiles
pipeline := $(dir $(abspath $(lastword $(MAKEFILE_LIST))))

include $(pipeline)/config.mk


#########################################
####### Required Input Parameters #######
#########################################
#                    NAME      Description
$(call var_required, NAME,     Output Name)
$(call var_required, GENOME,   Input Genome)
$(call var_required, LIBS,     Input Library Names)
$(call var_required, RARE,     Rare Cutter Name)
$(call var_required, FREQ,     Frequent Cutter Name)

# Set to single-threaded if CPUS isn't defined
CPUS?=1

.PHONY: all queue requeue
all: bwa egads coverage snps-all plink queue

queue:
	$(MAKE) $(MFLAGS) -j1 -f $(pipeline)/queue.mk all
requeue:
	$(MAKE) $(MFLAGS) -j1 -f $(pipeline)/queue.mk requeue-failed


################################################################################
## 0) Prepare Inputs
################################################################################
.PHONY: prepare
prepare: 0-ref/$(NAME).genome.fa 0-ref/$(NAME).genome.mk

0-ref/$(NAME).genome.mk: 0-ref/$(NAME).genome.fa.fai
	awk '{print "GENOME_SCAFFOLDS += " $$1}' FS="\t" $< > $@.tmp
	mv $@.tmp $@
-include 0-ref/$(NAME).genome.mk

0-ref/$(NAME).genome.fa : $(GENOME) | 0-ref/
	zcat -f $< > $@.tmp
	mv $@.tmp $@

################################################################################
## 1) BWA
################################################################################
.PHONY: bwa bwa-setup

### Create alignment file(s) from input lib name(s). Used in snps target as well
alignment_path = $(addprefix 1-bwa/$(NAME)/, $(addsuffix .bam, $(1)))
ALIGNMENTS := $(call alignment_path, $(LIBS))


bwa: $(ALIGNMENTS)
bwa-setup: 1-bwa/$(NAME)/db

### JOB ARRAY TARGETS bwa-run-#
$(call job-array-targets,bwa,$(ALIGNMENTS))

### Create database
1-bwa/$(NAME)/db : 0-ref/$(NAME).genome.fa | 1-bwa/$(NAME)/
	$(call sexec,$^,$(bwa)) bwa index -p $@ $<
	touch $@

### CAVEAT 1) Create [P]latform [U]nit from filename
PU = $(subst $(space),.,$(wordlist 3,4,$(subst _,$(space), $(1))))

.SECONDEXPANSION:
$(ALIGNMENTS): 1-bwa/$(NAME)/%.bam : 1-bwa/$(NAME)/db $$($$*)  | 1-bwa/$(NAME)/
	$(call sexec,$^,$(bwa)) bwa mem -t $(CPUS) \
			-R "@RG\tID:$*\tLB:$*\tSM:$*\tPL:illumina\tPU:$(call PU, $(word 2, $^))" \
			$^ \
	| singularity exec $(samtools) samtools fixmate -O bam - - \
	| singularity exec -B $(dir $@) $(samtools) samtools sort -T $@.tmp.tmp -o $@.tmp.bam -
	mv $@.tmp.bam $@

################################################################################
## 2) egads
################################################################################

.PHONY: egads
egads: 2-egads/$(NAME).bed

2-egads/$(NAME).bed: 0-ref/$(NAME).genome.fa | 2-egads/
	$(pipeline)/egads/egads --rare $(RARE) --freq $(FREQ) --genome $< --bed $@.tmp
	mv $@.tmp $@

################################################################################
## 3) Coverage Stats
################################################################################
GENOME_STATS := $(addprefix 3-coverage/$(NAME)/, $(addsuffix .genome.txt, $(LIBS)))
FRAGMENT_STATS := $(addprefix 3-coverage/$(NAME)/, $(addsuffix .fragment.txt, $(LIBS)))

.PHONY: coverage
coverage: $(GENOME_STATS) $(FRAGMENT_STATS)

$(GENOME_STATS): 3-coverage/$(NAME)/%.genome.txt: 1-bwa/$(NAME)/%.bam \
												| 3-coverage/$(NAME)/
	$(call sexec,$^,$(bedtools)) bedtools genomecov -ibam $< > $@.tmp
	mv $@.tmp $@

$(FRAGMENT_STATS): 3-coverage/$(NAME)/%.fragment.txt: 1-bwa/$(NAME)/%.bam \
												  	2-depthfinder/$(NAME).bed \
													| 3-coverage/$(NAME)/
	$(call sexec,$^,$(bedtools)) bedtools coverage -hist -a $(word 2, $^) -b $< > $@.tmp
	mv $@.tmp $@

################################################################################
## 4) SNP Calling
################################################################################

.PHONY: snps
snps: snps-all

snps-%: 0-ref/$(NAME).genome.fa 2-depthfinder/$(NAME).bed $(ALIGNMENTS) | 4-snps/
	$(MAKE) $(MFLAGS) -C $| -f $(pipeline)/4-snps.mk \
	GENOME=$(abspath $<) \
	LIBS="$(LIBS)" \
	FRAGS=$(abspath $(word 2, $^)) \
	CPUS=$(CPUS) NAME=$(NAME) \
	$(foreach lib,$(LIBS), $(lib)=$(abspath $(call alignment_path, $(lib)))) \
	$*

4-snps/$(NAME).g.vcf.gz: snps-merge

################################################################################
## 5) Plink
################################################################################

.PHONY: plink
plink: 5-plink/$(NAME)/plink.mds.png


5-plink/$(NAME)/plink.mds.png: 0-ref/$(NAME).genome.fa 4-snps/$(NAME).g.vcf.gz |  5-plink/$(NAME)/
	$(MAKE) $(MFLAGS) -C $| -f $(pipeline)/5-plink.mk \
	GENOME=$(abspath $<) \
	VCF=$(abspath $(word 2, $^)) all

# end
