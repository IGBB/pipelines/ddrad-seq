# Path to pipeline makefiles
pipeline := $(dir $(abspath $(lastword $(MAKEFILE_LIST))))

include $(pipeline)/config.mk

$(call var_required, NAME,     Output Name)
$(call var_required, GENOME,   Input Genome)
$(call var_required, LIBS,     Input Library Names)
$(call var_required, CPUS,     Number of threads to use for each task)

.PHONY: all
all: merge

include $(GENOME:.fa=.mk)

LIB_NUMS    := $(shell seq 1 $(words $(LIBS)))
GENOME_NUMS := $(shell seq 1 $(words $(GENOME_SCAFFOLDS)))


################################################################################
## 1) Mark Duplicates
################################################################################
MARKDUPS := $(addprefix 1-markdups/$(NAME)/, $(addsuffix .bam, $(LIBS)))

.PHONY: markdups
markdups: $(MARKDUPS)

## Make job array targets
$(call job-array-targets,markdups,$(MARKDUPS))

.SECONDEXPANSION:
$(MARKDUPS) : 1-markdups/$(NAME)/%.bam : $$($$*) | 1-markdups/$(NAME)/
	$(call sexec, $^, $(picard)) \
	picard MarkDuplicates I=$< O=$@.tmp.bam M=$@.metrics.txt
	rename $@.tmp.bam $@ $@.tmp.bam*

################################################################################
## 2) Prelim SNPs
################################################################################
.PHONY: prelim
prelim: 2-prelim/$(NAME).prelim.vcf.gz

2-prelim/$(NAME).bcf: $(GENOME) $(MARKDUPS) | 2-prelim/
	$(call sexec, $^, $(bcftools)) \
	bcftools mpileup -Ob -o $@.tmp -f $^
	mv $@.tmp $@

2-prelim/$(NAME).vcf.gz: 2-prelim/$(NAME).bcf | 2-prelim/
	$(call sexec, $^, $(bcftools)) \
	bcftools call -vmO z -o $@.tmp $<
	mv $@.tmp $@

2-prelim/$(NAME).prelim.vcf.gz: 2-prelim/$(NAME).vcf.gz | 2-prelim/
	$(call sexec, $^, $(bcftools)) \
	bcftools filter -O z -o $@.tmp -s LOWQUAL -i '%QUAL>10' $<
	mv $@.tmp $@

################################################################################
## 3) Base Recalibrator
################################################################################

BQSR_TBL := $(addprefix 3-bqsr/$(NAME)/, $(addsuffix .recal.tbl, $(LIBS)))
BQSR_BAM := $(addprefix 3-bqsr/$(NAME)/, $(addsuffix .bam, $(LIBS)))

.PHONY: bqsr bqsr-setup
bqsr: $(BQSR_BAM)


bqsr-setup: $(GENOME:.fa=.dict) 2-prelim/$(NAME).prelim.vcf.gz.tbi
## Make job array targets
$(call job-array-targets,bqsr,$(BQSR_BAM))

%.dict : %.fa
	$(call sexec, $<, $(gatk)) gatk CreateSequenceDictionary -R $<

$(BQSR_TBL): 3-bqsr/$(NAME)/%.recal.tbl : 1-markdups/$(NAME)/%.bam \
											$(GENOME) \
											2-prelim/$(NAME).prelim.vcf.gz \
											$(GENOME:.fa=.dict) $(GENOME).fai \
											2-prelim/$(NAME).prelim.vcf.gz.tbi \
										| 3-bqsr/$(NAME)/
	$(singularity) $(call container_binds, $^) $(gatk) \
	gatk BaseRecalibrator -I $< -R $(word 2,$^) --known-sites $(word 3,$^) -O $@.tmp
	mv $@.tmp $@

$(BQSR_BAM): 3-bqsr/$(NAME)/%.bam : 1-markdups/$(NAME)/%.bam \
									$(GENOME) \
									3-bqsr/$(NAME)/%.recal.tbl \
									| 3-bqsr/$(NAME)/
	$(call sexec, $^, $(gatk)) \
	gatk ApplyBQSR -I $< -R $(word 2,$^) --bqsr-recal-file $(word 3,$^) -O $@.tmp
	mv $@.tmp $@

################################################################################
## 4) HaplotypeCaller
################################################################################
HAPLO_VCF := $(addprefix 4-haplotype/$(NAME)/, $(addsuffix .g.vcf, $(LIBS)))

.PHONY: hyplotype
hyplotype: $(HAPLO_VCF)

## Make job array targets
$(call job-array-targets,haplotype,$(HAPLO_VCF))

$(HAPLO_VCF): 4-haplotype/$(NAME)/%.g.vcf :	3-bqsr/$(NAME)/%.bam \
												3-bqsr/$(NAME)/%.bam.bai \
												$(GENOME) \
											 |	4-haplotype/$(NAME)/
	$(call sexec, $^, $(gatk)) \
	gatk --java-options "-Xmx32g" HaplotypeCaller \
		 --native-pair-hmm-threads $(CPUS) \
		 -R $(word 3, $^) -I $< -O $@.tmp \
		 -ERC GVCF --sample-name $*
	rename $@.tmp $@ $@.tmp*

################################################################################
## 5) Combine
################################################################################
GENOME_DB=$(addprefix 5-combine/$(NAME)/,$(addsuffix .db, $(GENOME_SCAFFOLDS)))

.PHONY: combine combine-run-setup
combine: $(GENOME_DB)
combine-run-setup: 5-combine/$(NAME)/sample.map
## Make job array targets
$(call job-array-targets,combine,$(GENOME_DB))


5-combine/$(NAME)/sample.map: $(HAPLO_VCF) | 5-combine/$(NAME)/
	awk '{lib=$$1; sub(".*/", "", lib); sub(".g.vcf", "", lib); printf "%s\t%s\n", lib, $$1 }' RS=' ' <<<"$(abspath $^)" > $@

$(GENOME_DB): 5-combine/$(NAME)/%.db: 5-combine/$(NAME)/sample.map $(HAPLO_VCF) | 5-combine/$(NAME)/tmp/
	$(call sexec, $^, $(gatk)) \
	gatk --java-options "-Xmx32g -Xms32g" GenomicsDBImport \
	--genomicsdb-workspace-path $|/$*.db \
	--batch-size $(words $(LIBS)) \
	-L $* \
	--sample-name-map $< \
	--tmp-dir $| \
	--reader-threads $(CPUS)
	mv $|/$*.db $@

################################################################################
## 6) Genotype
################################################################################
GENOME_VCF=$(addprefix 6-genotype/$(NAME)/,$(addsuffix .vcf.gz, $(GENOME_SCAFFOLDS)))

.PHONY: genotype
genotype: $(GENOME_VCF)

## Make job array targets
$(call job-array-targets,genotype,$(GENOME_VCF))


$(GENOME_VCF): 6-genotype/$(NAME)/%.vcf.gz: 5-combine/$(NAME)/%.db $(GENOME) | 6-genotype/$(NAME)/
	$(call sexec, $^, $(gatk)) \
	gatk --java-options "-Xmx32g -Xms32g" GenotypeGVCFs \
		-R $(GENOME) -V gendb://$(abspath $<) -O $@.tmp.g.vcf.gz
	rename $@.tmp.g.vcf.gz $@ $@.tmp.g.vcf.gz*

################################################################################
## 7) Merge
################################################################################

.PHONY: merge
merge: $(NAME).g.vcf.gz

$(NAME).g.vcf.gz: $(GENOME_VCF)
	$(call sexec,$^,$(bcftools)) bcftools concat $^ -Oz -o $@.tmp.vcf.gz
	mv $@.tmp.vcf.gz $@
