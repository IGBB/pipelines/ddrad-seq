#### Global Config Variables and Rules

# Path to pipeline makefiles
pipeline := $(dir $(abspath $(lastword $(MAKEFILE_LIST))))



# Create empty variable
empty:=
# Create variable with single ' '
space:=$(empty) $(empty)



#MUST HAVE FOR MODULES TO WORK W/O WARNINGS
SHELL := /bin/bash
export LC_ALL=C

# Containers
bwa      := $(pipeline)/bwa-0.7.17.sif
samtools := $(pipeline)/samtools-1.13.sif
bedtools := $(pipeline)/bedtools-2.30.0.sif
picard   := $(pipeline)/picard-2.26.3.sif
bcftools := $(pipeline)/bcftools-1.13.sif
gatk     := $(pipeline)/gatk-4.2.2.0.sif
vcftools := $(pipeline)/vcftools-0.1.16.sif

# Add bins fo real and absolute path to singularity
container_binds = $(addprefix -B, $(sort $(dir $(realpath $(1)) $(abspath $(1)))))

sload       := ml singularity/3.7.1
singularity := $(sload) && singularity exec
sexec = $(singularity) $(call container_binds, $1 .) $2

# Automatically create sub-directories
%/:
	mkdir -p $@

# Index bam files
%.bai: %
	$(singularity) $(call container_binds, $<) $(samtools) \
	samtools index $<

# Index fasta files
%.fai: %
	$(singularity) $(call container_binds, $<) $(samtools) \
	samtools faidx $<

# Index vcf files
%.tbi: %
	ml htslib && tabix -p vcf $<

# Check that given variables are set and all have non-empty values,
# die with an error otherwise.
#
# Params:
#   1. Variable name(s) to test.
#   2. (optional) Error message to print.
var_required = \
	$(strip $(foreach 1,$1, \
		$(call __var_required,$1,$(strip $(value 2)))))
__var_required = \
	$(if $(value $1),, \
		$(error Undefined $1$(if $2, ($2))))

################################################################################
### JOB ARRAY TARGETS
### Creates phony numbered targets named "$(prefix)-run-$(#)" for a given list
###	REQUIRES:
###	    1) prefix of phony target
###	    2) list of files
### USAGE :
###     $(call job-array-targets,markdups,$(MARKDUPS))
###
################################################################################
define __job-array-targets
.PHONY: $(strip $1)-run-$(strip $2)
$(strip $1)-run-$(strip $2): $(word $2, $3)
endef
job-array-targets = $(foreach num,$(shell seq 1 $(words $2)), \
						$(eval $(call __job-array-targets,$(strip $1),$(num),$2)))
