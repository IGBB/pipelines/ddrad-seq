
# Path to pipeline makefiles
pipeline := $(dir $(abspath $(lastword $(MAKEFILE_LIST))))

include $(pipeline)/config.mk


#########################################
####### Required Input Parameters #######
#########################################
#                    NAME      Description
$(call var_required, GENOME,   Input Genome)
$(call var_required, VCF,   Input Genome)

.PHONY: all
all: plink.mds.png

chrom-map: $(abspath $(GENOME)).fai
	awk '{print $$1, NR}' OFS="\t" FS="\t" $< > $@

study: $(abspath $(VCF)) chrom-map
	$(call sexec,$^,$(vcftools)) vcftools --gzvcf $< --plink --chrom-map $(word 2, $^) --out $@
	touch $@

study.plink.genome: study.plink
study.plink: study
	$(pipeline)/plink-1.07-x86_64/plink --file $< --genome --noweb --allow-no-sex --out $@
	touch $@

plink.mds: plink
plink: study study.plink.genome
	$(pipeline)/plink-1.07-x86_64/plink \
		--file $(word 1,$^) \
		--read-genome $(word 2,$^) \
		--cluster \
		--mds-plot 2 \
		--noweb \
		--out $@
	touch $@

plink.mds.png: plink.mds
	ml r/4.0.2 && Rscript $(pipeline)/5-plink.mds.R $<
